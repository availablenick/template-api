const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const { v4: uuid } = require("uuid");

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (req, res) => res.json("Hello, World!"));

  /* ******************* */
  /* YOUR CODE GOES HERE */
  /* ******************* */

  // Creation
  api.post("/users", async (req, res) => {
    let acceptedKeys = ["name", "email", "password", "passwordConfirmation"];
    for (let key of acceptedKeys) {
      if (!req.body[key]) {
        return res.status(400).json({
          error: "Request body had missing field " + key
        });
      }
    }

    // Name
    let re = /([a-zA-Z] *)+/i;
    if (!re.test(req.body.name)) {
      return res.status(400).json({
        error: "Request body had malformed field name"
      });
    }

    // E-mail
    re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!re.test(req.body.email)) {
      return res.status(400).json({
        error: "Request body had malformed field email"
      });
    }

    // Password
    re = /[a-zA-z0-9]{8,32}/;
    if (!re.test(req.body.password)) {
      return res.status(400).json({
        error: "Request body had malformed field password"
      });
    }

    // Password Confirm
    if (!re.test(req.body.passwordConfirm)) {
      return res.status(400).json({
        error: "Request body had malformed field passwordConfirmation"
      });
    }

    // Check match
    if (req.body.passwordConfirmation !== req.body.password) {
      return res.status(422).json({
        error: "Password confirmation did not match"
      });
    }

    // Publish
    stanConn.publish('users', 'UserCreated', () => {});

    // Fetch user (no effect)
    mongoClient.db().collection().findOne();
    
    // Success
    return res.status(201).json({
      user: {
        id: uuid(),
        name: req.body.name,
        email: req.body.email
      }
    });
  });

  // Deletion
  api.delete("/users/:uuid", (req, res) => {
    let authorizationHeader = req.get("Authentication");
    if (!authorizationHeader) {
      return res.status(401).json({
        error: "Access Token not found"
      });
    }

    // Retrieve UUID from token
    let match = /Bearer (.+)/.exec(authorizationHeader);
    let token = match[1];
    let decoded = jwt.verify(token, secret);
    if (decoded.id !== req.params.uuid) {
      return res.status(403).json({
        error: "Access Token did not match User ID"
      });
    }

    // Publish
    stanConn.publish('users', 'UserDeleted', () => {});
    
    return res.status(200).json({
      id: req.params.uuid
    });
  });

  return api;
};
